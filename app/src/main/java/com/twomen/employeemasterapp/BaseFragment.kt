package com.twomen.employeemasterapp

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.employee_master.*

open class BaseFragment : Fragment() {

    lateinit var sharedPreferencesManager: SharedPreferencesManager

    fun setEmployeeIdPreferences(employeeId: Int) {
        sharedPreferencesManager.employeeId = employeeId
    }

    fun setHistoryIdPreferences(historyId: Int) {
        sharedPreferencesManager.historyId = historyId
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        context?.let {
            sharedPreferencesManager = SharedPreferencesManager(it)
        }
    }

    fun fragmentTransaction(fragment: Fragment) {
        fragmentManager?.beginTransaction()
                ?.replace(R.id.fragmentContainer, fragment)
                ?.commit()
    }
}