package com.twomen.employeemasterapp

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesManager(context: Context) : ISharedPreferencesManager {

    private val sharedPreferences = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)

    companion object {
        const val EMPLOYEE_ID = "employee_id"
        const val HISTORY_ID = "history_id"
        const val PREFS_FILENAME = "com.twomen.employeemasterapp.util.prefs"
    }

    override var employeeId: Int
        get() {
            return sharedPreferences.getInt(EMPLOYEE_ID, 0)
        }
        set(value) {
            storeValue(EMPLOYEE_ID, value)
        }

    override var historyId: Int
        get() {
            return sharedPreferences.getInt(HISTORY_ID, 0)
        }
        set(value) {
            storeValue(HISTORY_ID, value)
        }

    private fun storeValue(key: String, int: Int) =
            sharedPreferences
                    .edit()
                    .putInt(key, int)
                    .apply()

    private fun storeValue(key: String, long: Long) =
            sharedPreferences
                    .edit()
                    .putLong(key, long)
                    .apply()

    private fun storeValue(key: String, string: String) =
            sharedPreferences
                    .edit()
                    .putString(key, string)
                    .apply()
}