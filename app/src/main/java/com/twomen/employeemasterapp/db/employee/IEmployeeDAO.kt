package com.twomen.employeemasterapp.db.employee

import com.twomen.employeemasterapp.db.RealmLiveData
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryObject
import io.realm.RealmResults

interface IEmployeeDAO {
    fun findById(id: Int): EmployeeObject?
    fun findAll(): RealmResults<EmployeeObject?>
    fun save(employee: EmployeeObject, employeeId: Int)

}