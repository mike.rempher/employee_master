package com.twomen.employeemasterapp.db.employeehistory

import com.twomen.employeemasterapp.db.employee.EmployeeObject
import com.twomen.employeemasterapp.db.employee.IEmployeeDAO
import io.realm.Realm
import io.realm.RealmResults

class EmployeeHistoryDAO constructor(private val realm: Realm) : IEmployeeHistoryDAO {

    override fun findById(id: Int): EmployeeHistoryObject? = realm.where(EmployeeHistoryObject::class.java)
            .equalTo("id", id)
            .findFirst()

    override fun findAll(): RealmResults<EmployeeHistoryObject?> = realm.where(EmployeeHistoryObject::class.java)
            .findAll()
}