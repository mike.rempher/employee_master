package com.twomen.employeemasterapp.db.address

import io.realm.Realm
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class AddressObject(@PrimaryKey var id: Int = 0,
                         var employeeId: Int = 0,
                         var addressLine1: String = "",
                         var addressLine2: String = "",
                         var apartmentSuite: String = "",
                         var city: String = "",
                         var state: String = "",
                         var zipCode: Long = 0

) : RealmObject() {

    fun setUpObject() {
        val realm = Realm.getDefaultInstance()

        val address1 = AddressObject(
                id = 1,
                employeeId = 1,
                addressLine1 = "1450 W Mt Hope Ave",
                city = "Lansing",
                state = "Michigan",
                zipCode = 48910
        )
        val address2 = AddressObject(
                id = 2,
                employeeId = 2,
                addressLine1 = "2122 E Albert Ave",
                addressLine2 = "Apt 2.",
                city = "Holt",
                state = "Michigan",
                zipCode = 48842
        )

        realm.executeTransaction {
            realm.copyToRealmOrUpdate(address1)
            realm.copyToRealmOrUpdate(address2)
        }
    }
}