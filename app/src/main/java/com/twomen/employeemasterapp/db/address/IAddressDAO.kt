package com.twomen.employeemasterapp.db.address

import com.twomen.employeemasterapp.db.employee.EmployeeObject
import io.realm.RealmResults


interface IAddressDAO {
    fun findById(id: Int): AddressObject?
    fun findAll(): RealmResults<AddressObject?>
}


