package com.twomen.employeemasterapp.db.employee

import com.twomen.employeemasterapp.R.drawable.profile
import io.realm.Realm
import io.realm.RealmResults

class EmployeeDAO constructor(private val realm: Realm) : IEmployeeDAO {

    override fun findById(id: Int): EmployeeObject? = realm.where(EmployeeObject::class.java)
            .equalTo("id", id)
            .findFirst()


    override fun findAll(): RealmResults<EmployeeObject?> = realm.where(EmployeeObject::class.java)
            .findAll()

    override fun save(employee: EmployeeObject, employeeId: Int) {
        realm.where(EmployeeObject::class.java)
                .equalTo("ID", employeeId)
                .findFirst()

        realm.executeTransaction{ it.copyToRealmOrUpdate(employee) }

    }
}