package com.twomen.employeemasterapp.db

import java.util.*

open class BaseData {

    var year = 0
    var month = 0
    var day = 0

    fun setDate(year: Int, month: Int, day: Int): Date {
        val calendar: Calendar = Calendar.getInstance()
        val date: Date
        if (this.year != year) {
            this.year = year
        }
        if (this.month != month) {
            this.month = month
        }
        if (this.day != day) {
            this.day = day
        }
        calendar.set(year, month, day)
        date = calendar.time

        return date
    }

}