package com.twomen.employeemasterapp.db.employeehistory

import com.twomen.employeemasterapp.db.employee.EmployeeObject
import io.realm.RealmResults

interface IEmployeeHistoryDAO {
    fun findById(id: Int) : EmployeeHistoryObject?
    fun findAll(): RealmResults<EmployeeHistoryObject?>
}