package com.twomen.employeemasterapp.db.address

import com.twomen.employeemasterapp.db.employee.EmployeeObject
import io.realm.Realm
import io.realm.RealmResults

class AddressDAO constructor(private val realm: Realm) : IAddressDAO {

    override fun findById(id: Int): AddressObject? = realm.where(AddressObject::class.java)
            .equalTo("id", id)
            .findFirst()


    override fun findAll(): RealmResults<AddressObject?> = realm.where(AddressObject::class.java)
            .findAll()

}