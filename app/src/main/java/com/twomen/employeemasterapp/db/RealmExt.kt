package com.twomen.employeemasterapp.db

import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.RealmResults

fun <T: RealmModel> RealmResults<T>.asLiveData() = RealmLiveData<T>(this)


@Suppress("UNCHECKED_CAST")
fun <T> Realm.genUniqueId(`class`: Class<T>): Int {
    val currentId = this.where(`class` as Class<RealmObject>).max("id")
    return if (currentId == null) 1 else currentId.toInt() + 1 //auto increment for unique id
}