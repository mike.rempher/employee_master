package com.twomen.employeemasterapp.db.employeehistory

import com.twomen.employeemasterapp.db.BaseData
import io.realm.Realm
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class EmployeeHistoryObject(@PrimaryKey var id: Int = 0,
                                 var employeeId: Int = 0,
                                 var beginDate: Date = Date(0),
                                 var endDate: Date? = Date(0),
                                 var terminationDate: Date? = Date(0),
                                 var terminationReason: String = "",
                                 var isVoluntaryTermination: String = "",
                                 var canHire: Boolean = false,
                                 var modifiedBy: String = "",
                                 var modifiedDateTime: Date = Date(0),
                                 var employeeOUId: Int = 0
) : RealmObject() {

    fun setUpObject() {
        val realm = Realm.getDefaultInstance()

        val employeeHistory1 = EmployeeHistoryObject(
                id = 1,
                employeeId = 1,
                beginDate = BaseData().setDate(2015, 0, 1),
                endDate = null,
                terminationDate = BaseData().setDate(2018, 0, 1),
                terminationReason = "Transfer",
                isVoluntaryTermination = "Voluntary",
                canHire = true,
                modifiedBy = "Mike.Rempher",
                modifiedDateTime = BaseData().setDate(2015, 0, 1),
                employeeOUId = 10001
        )

        val employeeHistory2 = EmployeeHistoryObject(
                id = 2,
                employeeId = 1,
                beginDate = BaseData().setDate(2018, 0, 1),
                endDate = null,
                terminationDate = BaseData().setDate(2018, 5, 1),
                terminationReason = "Stealing",
                isVoluntaryTermination = "Involuntary",
                canHire = false,
                modifiedBy = "Mike.Rempher",
                modifiedDateTime = BaseData().setDate(2018, 0, 1),
                employeeOUId = 10001
        )

        val employeeHistory3 = EmployeeHistoryObject(
                id = 3,
                employeeId = 2,
                beginDate = BaseData().setDate(2016, 0, 1),
                endDate = BaseData().setDate(2017, 5, 1),
                terminationDate = BaseData().setDate(2016, 5, 1),
                terminationReason = "Quit",
                isVoluntaryTermination = "Voluntary",
                canHire = true,
                modifiedBy = "Mike.Rempher",
                modifiedDateTime = BaseData().setDate(2016, 0, 1),
                employeeOUId = 10001
        )

        val employeeHistory4 = EmployeeHistoryObject(
                id = 4,
                employeeId = 2,
                beginDate = BaseData().setDate(2017, 0, 1),
                endDate = null,
                terminationDate = BaseData().setDate(2017, 5, 1),
                terminationReason = "Quit",
                isVoluntaryTermination = "Voluntary",
                canHire = true,
                modifiedBy = "Mike.Rempher",
                modifiedDateTime = BaseData().setDate(2017, 0, 1),
                employeeOUId = 10001
        )

        realm.executeTransaction {
            realm.copyToRealmOrUpdate(employeeHistory1)
            realm.copyToRealmOrUpdate(employeeHistory2)
            realm.copyToRealmOrUpdate(employeeHistory3)
            realm.copyToRealmOrUpdate(employeeHistory4)
        }
    }
}