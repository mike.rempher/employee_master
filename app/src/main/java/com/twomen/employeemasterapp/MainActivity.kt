package com.twomen.employeemasterapp

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.twomen.employeemasterapp.db.address.AddressObject
import com.twomen.employeemasterapp.db.employee.EmployeeObject
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryObject
import com.twomen.employeemasterapp.fragments.EmployeeDetailsFragment
import com.twomen.employeemasterapp.fragments.employeeactivity.EmployeeActivityFragment
import com.twomen.employeemasterapp.fragments.employeeavailability.EmployeeAvailabilityFragment
import com.twomen.employeemasterapp.fragments.employeehomepage.EmployeeListFragment
import com.twomen.employeemasterapp.fragments.employeejobinfo.EmployeeJobInfoFragment
import com.twomen.employeemasterapp.fragments.employeeskills.EmployeeSkillsFragment
import com.twomen.employeemasterapp.models.modules.CustomModelsModule
import io.realm.Realm
import io.realm.RealmConfiguration
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    var fragment: Fragment = EmployeeListFragment()
    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        /*
        This method is called to toggle the Navigation Drawer whenever
        the Hamburger icon is clicked in the ActionBar/Toolbar
        */
        val toggle = ActionBarDrawerToggle(
                this, /* host activity */
                drawer_layout, /* Drawer Layout object */
                toolbar, /* Toolbar Object */
                R.string.nav_open, /* description for accessibility */
                R.string.nav_closed  /* description for accessibility */
        )

        // Set-up the NavigationView and its listener
        nav_view.setNavigationItemSelectedListener(this)
        // This will load HomeFragment on activity Startup
        fragmentTransaction(EmployeeListFragment())
        add_employee_fab.setOnClickListener(this)
        // Set the drawer toggle as the DrawerListener
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.menu.findItem(R.id.nav_home).isVisible = false
        nav_view.menu.findItem(R.id.nav_employee_master).isVisible = false
        nav_view.menu.findItem(R.id.nav_availability).isVisible = false
        nav_view.menu.findItem(R.id.nav_job_info).isVisible = false
        nav_view.menu.findItem(R.id.nav_activity_type).isVisible = false
        nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = false
        nav_view.menu.findItem(R.id.nav_employee_details).isVisible = false

        setupConfig()
        realm = Realm.getDefaultInstance()
        if (realm.isEmpty) {
            EmployeeObject().setUpObject()
            AddressObject().setUpObject()
            EmployeeHistoryObject().setUpObject()

        }
    }

    private fun setupConfig() {
        val config = RealmConfiguration.Builder()
                .name("employeemaster.realm")
                .modules(CustomModelsModule())
                .schemaVersion(3)
                .build()

        Realm.setDefaultConfiguration(config)
    }

    override fun onClick(v: View?) {
        fragmentTransaction(EmployeeMaster())
        add_employee_fab.hide()
        nav_view.menu.findItem(R.id.nav_home).isVisible = true
        nav_view.menu.findItem(R.id.nav_employee_master).isVisible = false
        nav_view.menu.findItem(R.id.nav_availability).isVisible = true
        nav_view.menu.findItem(R.id.nav_job_info).isVisible = true
        nav_view.menu.findItem(R.id.nav_activity_type).isVisible = true
        nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = true
        nav_view.menu.findItem(R.id.nav_employee_details).isVisible = true
    }

    // Called when an item in the navigation menu is selected.
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        loadFragment(id)
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    // This method will take a fragment and add/replace
    // that fragment to the activity
    fun fragmentTransaction(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
    }

    // This method will load fragment based on
    // the menu id
    private fun loadFragment(menuId: Int) {
        when (menuId) {
            R.id.nav_home -> {
                fragment = EmployeeListFragment()
                nav_view.menu.findItem(R.id.nav_home).isVisible = false
                nav_view.menu.findItem(R.id.nav_employee_master).isVisible = false
                nav_view.menu.findItem(R.id.nav_availability).isVisible = false
                nav_view.menu.findItem(R.id.nav_job_info).isVisible = false
                nav_view.menu.findItem(R.id.nav_activity_type).isVisible = false
                nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = false
                nav_view.menu.findItem(R.id.nav_employee_details).isVisible = false
                add_employee_fab.show()
            }
            R.id.nav_employee_master -> {
                fragment = EmployeeMaster()
                nav_view.menu.findItem(R.id.nav_home).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_master).isVisible = false
                nav_view.menu.findItem(R.id.nav_availability).isVisible = true
                nav_view.menu.findItem(R.id.nav_job_info).isVisible = true
                nav_view.menu.findItem(R.id.nav_activity_type).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_details).isVisible = true
                add_employee_fab.hide()
            }
            R.id.nav_availability -> {
                fragment = EmployeeAvailabilityFragment()
                nav_view.menu.findItem(R.id.nav_home).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_master).isVisible = true
                nav_view.menu.findItem(R.id.nav_availability).isVisible = false
                nav_view.menu.findItem(R.id.nav_job_info).isVisible = true
                nav_view.menu.findItem(R.id.nav_activity_type).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_details).isVisible = true
                add_employee_fab.hide()
            }
            R.id.nav_job_info -> {
                fragment = EmployeeJobInfoFragment()
                nav_view.menu.findItem(R.id.nav_home).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_master).isVisible = true
                nav_view.menu.findItem(R.id.nav_availability).isVisible = true
                nav_view.menu.findItem(R.id.nav_job_info).isVisible = false
                nav_view.menu.findItem(R.id.nav_activity_type).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_details).isVisible = true
                add_employee_fab.hide()
            }
            R.id.nav_activity_type -> {
                fragment = EmployeeActivityFragment()
                nav_view.menu.findItem(R.id.nav_home).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_master).isVisible = true
                nav_view.menu.findItem(R.id.nav_availability).isVisible = true
                nav_view.menu.findItem(R.id.nav_job_info).isVisible = true
                nav_view.menu.findItem(R.id.nav_activity_type).isVisible = false
                nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_details).isVisible = true
                add_employee_fab.hide()
            }
            R.id.nav_employee_skills -> {
                fragment = EmployeeSkillsFragment()
                nav_view.menu.findItem(R.id.nav_home).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_master).isVisible = true
                nav_view.menu.findItem(R.id.nav_availability).isVisible = true
                nav_view.menu.findItem(R.id.nav_job_info).isVisible = true
                nav_view.menu.findItem(R.id.nav_activity_type).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = false
                nav_view.menu.findItem(R.id.nav_employee_details).isVisible = true
                add_employee_fab.hide()
            }
            R.id.nav_employee_details -> {
                fragment = EmployeeDetailsFragment()
                nav_view.menu.findItem(R.id.nav_home).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_master).isVisible = true
                nav_view.menu.findItem(R.id.nav_availability).isVisible = true
                nav_view.menu.findItem(R.id.nav_job_info).isVisible = true
                nav_view.menu.findItem(R.id.nav_activity_type).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_skills).isVisible = true
                nav_view.menu.findItem(R.id.nav_employee_details).isVisible = false
                add_employee_fab.hide()
            }
            R.id.nav_exit -> exitProcess(0)
        }
        // Fragment Transaction
        fragmentTransaction(fragment)
    }
}


