package com.twomen.employeemasterapp.models.modules

import com.twomen.employeemasterapp.db.address.AddressObject
import com.twomen.employeemasterapp.db.employee.EmployeeObject
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryObject
import io.realm.annotations.RealmModule

@RealmModule(
        library = true,
        classes = [
            EmployeeObject::class,
            AddressObject::class,
            EmployeeHistoryObject::class
        ]
)
open class CustomModelsModule