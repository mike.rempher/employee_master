package com.twomen.employeemasterapp.fragments.employeeavailability

import android.R
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.twomen.employeemasterapp.*
import com.twomen.employeemasterapp.databinding.FragmentEmployeeAddressBinding
import com.twomen.employeemasterapp.databinding.FragmentEmployeeTimeOffRequestBinding
import com.twomen.employeemasterapp.db.address.AddressDAO
import com.twomen.employeemasterapp.fragments.employeemaster.employeeaddress.EmployeeAddressViewModel
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_employee_availability.*
import kotlinx.android.synthetic.main.fragment_employee_time_off_request.*

/**
 * Created by miker on 3/7/18.
 */
class EmployeeTimeOff : BaseFragment() {
    var TAG = "EmployeeTimeOff"

    lateinit var binding: FragmentEmployeeTimeOffRequestBinding
    private lateinit var addressViewModel: EmployeeAddressViewModel
    var realm: Realm = Realm.getDefaultInstance()

    lateinit var spinner: Spinner
    private lateinit var requestReason: Array<String>
    private lateinit var listAdapter: ExpandableListAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        requestReason = resources.getStringArray(com.twomen.employeemasterapp.R.array.time_off)
        listAdapter = ExpandableListAdapter(activity!!.applicationContext)

        val employeeId = sharedPreferencesManager.employeeId
        addressViewModel = EmployeeAddressViewModel(employeeId)

        return inflater.inflate(com.twomen.employeemasterapp.R.layout.fragment_employee_time_off_request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getReason(spinner)
        setSpinnerAdapter()
        binding.setVariable(BR.address, addressViewModel)
    }

    fun setSpinnerAdapter() {
        spinner = this.request_reason

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(activity, R.layout.simple_spinner_item, resources.getStringArray(com.twomen.employeemasterapp.R.array.time_off))
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // Set Adapter to Spinner
        spinner.adapter = aa

        setSpinner(spinner, sharedPreferencesManager.employeeId)

    }

    private fun setSpinner(spinner: Spinner, employeeId: Int) {
        val reason = AddressDAO(realm).findById(employeeId)?.state
        spinner.setSelection(addressViewModel.getIndex(requestReason, reason))
    }

    private fun getReason(spinner: Spinner?): String {
        var state = "no state"

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //TODO
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                state = p0?.getItemAtPosition(p2).toString()
            }
        }
        return state
    }
}