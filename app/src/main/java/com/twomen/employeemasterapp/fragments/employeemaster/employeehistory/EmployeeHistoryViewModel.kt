package com.twomen.employeemasterapp.fragments.employeemaster.employeehistory

import android.databinding.BaseObservable
import android.databinding.Observable
import android.databinding.ObservableField
import android.util.Log
import android.widget.Checkable
import com.twomen.employeemasterapp.R.id.radioGroup
import com.twomen.employeemasterapp.R.id.term_type_voluntary
import com.twomen.employeemasterapp.db.employee.EmployeeDAO
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryDAO
import io.realm.Realm
import java.text.SimpleDateFormat
import java.util.*

class EmployeeHistoryViewModel(employeeId: Int): BaseObservable() {
    val realm: Realm = Realm.getDefaultInstance()
    private val historyDAO: EmployeeHistoryDAO = EmployeeHistoryDAO(realm)
    private val formatter = SimpleDateFormat("yyyy/MM/dd", Locale.US)

    var beginDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var termDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var termReason = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var termType = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var rehire = ObservableField<Boolean>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    init{
        beginDate.set(formatter.format(historyDAO.findById(employeeId)?.beginDate ?: ""))
        termDate.set(formatter.format(historyDAO.findById(employeeId)?.terminationDate ?: ""))
        termReason.set(historyDAO.findById(employeeId)?.terminationReason ?: "")
        termType.set(historyDAO.findById(employeeId)?.isVoluntaryTermination ?: "")
        setRehire(employeeId)

    }

    fun setRehire(employeeId: Int) {
        val canRehire = historyDAO.findById(employeeId)?.canHire ?: false
        rehire.set(canRehire)
    }
}