package com.twomen.employeemasterapp.fragments.employeehomepage

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.db.employee.EmployeeObject
import io.realm.OrderedRealmCollection
import io.realm.Realm


class CustomAdapter(private var employeeName: OrderedRealmCollection<EmployeeObject>)
    : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

    var onClickedListener: EmployeeAdapterOnClick? = null
    lateinit var realm: Realm

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.employee_list_info,
                parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int = employeeName.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        realm = Realm.getDefaultInstance()

        holder.firstName.text = employeeName[position].firstname
        holder.lastName.text = employeeName[position].lastname + " ,"

        holder.itemView.callOnClick()

        holder.itemView.setOnClickListener {
            val pos = holder.adapterPosition
            val employee = employeeName[pos]

            onClickedListener?.onClicked(employee)
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var firstName = itemView.findViewById(R.id.employee_firstName) as TextView
            var lastName = itemView.findViewById(R.id.employee_lastName) as TextView
    }
}