package com.twomen.employeemasterapp.fragments.employeeactivity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.twomen.employeemasterapp.R
import kotlinx.android.synthetic.main.fragment_employee_activity.*
import kotlinx.android.synthetic.main.layout_fab_submenu.*

class EmployeeActivityFragment: Fragment(){

    //boolean flag to know if main FAB is in open or closed state.
    private var fabExpanded: Boolean = false
    private lateinit var layoutAdd: LinearLayout
    private lateinit var layoutRemove: LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_employee_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layoutAdd = fabFrame.findViewById(R.id.layoutFabAdd)
        layoutRemove = fabFrame.findViewById(R.id.layoutFabRemove)

        closeSubMenusFab()
        add_remove_fab.setOnClickListener {
            when (fabExpanded) {
                true -> {
                    openSubMenusFab()
                }
                false -> {
                    closeSubMenusFab()
                }
            }
        }

        fabAdd.setOnClickListener {
            AddEmployeeDialogFragment().show(childFragmentManager, "TAG")
        }

        fabRemove.setOnClickListener {
            RemoveEmployeeDialogFragment().show(childFragmentManager, "TAG")
        }
        activity?.title = "EmployeeObject Activity"
    }

    //Opens FAB submenus
    private fun openSubMenusFab() {
        layoutAdd.visibility = View.VISIBLE
        layoutRemove.visibility = View.VISIBLE
        add_remove_fab.setImageResource(R.drawable.ic_close_black_24dp)
        fabExpanded = false
    }

    //closes FAB submenus
    private fun closeSubMenusFab() {
        layoutAdd.visibility = View.INVISIBLE
        layoutRemove.visibility = View.INVISIBLE
        add_remove_fab.setImageResource(R.drawable.ic_add_black_24dp)
        fabExpanded = true
    }

    companion object {
        const val TAG = "EmployeeActivityFragment"
    }
}
