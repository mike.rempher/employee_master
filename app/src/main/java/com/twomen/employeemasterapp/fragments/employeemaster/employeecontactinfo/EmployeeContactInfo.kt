package com.twomen.employeemasterapp.fragments.employeemaster.employeecontactinfo


import android.content.Context
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.*
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.EMPLOYEE_ID
import com.twomen.employeemasterapp.databinding.EmployeeEmergencyContactBinding
import io.realm.Realm


class EmployeeContactInfo : BaseFragment() {
    var TAG: String = "EmployeeContactInfo"


    lateinit var binding: EmployeeEmergencyContactBinding
    private lateinit var contactViewModel: EmployeeContactViewModel
    var realm: Realm = Realm.getDefaultInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val employeeId = sharedPreferencesManager.employeeId
        contactViewModel = EmployeeContactViewModel(employeeId)

        binding = DataBindingUtil.inflate(inflater, R.layout.employee_emergency_contact, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.setVariable(BR.emergancy, contactViewModel)

        activity?.title = "Employee Emergency Contact"
    }

}
