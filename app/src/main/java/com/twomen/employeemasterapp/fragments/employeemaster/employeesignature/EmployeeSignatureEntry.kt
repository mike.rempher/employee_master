package com.twomen.employeemasterapp.fragments.employeemaster.employeesignature

import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.github.gcacace.signaturepad.views.SignaturePad
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.databinding.SignaturePadBinding
import kotlinx.android.synthetic.main.signature_pad.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

open class EmployeeSignatureEntry : BaseFragment() {
    var TAG: String = "EmployeeSignatureEntry"

    lateinit var binding: SignaturePadBinding
    var employeeId: Int = 0
    private lateinit var signaturePad: SignaturePad

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.signature_pad, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        signaturePad = signature_pad

        signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
                Toast.makeText(context, "start", Toast.LENGTH_SHORT).show()
            }

            override fun onSigned() {
                save_button.isEnabled = true
                clear_button.isEnabled = true
            }

            override fun onClear() {
                save_button.isEnabled = false
                clear_button.isEnabled = false
            }
        })

        clear_button.setOnClickListener {
            signaturePad.clear()
        }

        save_button.setOnClickListener {
            val signatureBitMap = signaturePad.signatureBitmap
            saveImage(signatureBitMap)
            Toast.makeText(this.context, "Signature saved.", Toast.LENGTH_LONG).show()
            fragmentTransaction(EmployeeSignature())
        }
    }

    private fun saveImage(signature: Bitmap) {
        // the directory where the signature will be saved
        val myDir = File("Documents/saved_signature")

        // make the directory if it does not exist yet
        if (!myDir.exists()) {
            myDir.mkdirs()
        }

        // set the file name of your choice
        val fname = "signature.png"

        // in our case, we delete the previous file, you can remove this
        val file = File(myDir, fname)
        if (file.exists()) {
            file.delete()
        }

        try {
            // save the signature
            val out = FileOutputStream(file)
            signature.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.flush()
            out.close()

            Toast.makeText(this.context, "Signature saved.", Toast.LENGTH_LONG).show()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}