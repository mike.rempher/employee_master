package com.twomen.employeemasterapp.fragments.employeemaster.employeelicense


import android.content.Context
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.twomen.employeemasterapp.*
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.EMPLOYEE_ID
import com.twomen.employeemasterapp.databinding.FragmentEmployeeAddressBinding
import com.twomen.employeemasterapp.databinding.FragmentEmployeeLicenseBinding
import com.twomen.employeemasterapp.db.address.AddressDAO
import com.twomen.employeemasterapp.fragments.employeemaster.employeeaddress.EmployeeAddressViewModel
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_employee_address.*
import kotlinx.android.synthetic.main.fragment_employee_license.*

class EmployeeLicenseFragment: BaseFragment() {
    var TAG: String = "EmployeeLicenseFragment"

    lateinit var binding: FragmentEmployeeLicenseBinding
    private lateinit var licenseViewModel: EmployeeLicenseViewModel
    var realm: Realm = Realm.getDefaultInstance()
    private lateinit var issueState: Array<String>
    private var spinner: Spinner? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true)

        issueState = resources.getStringArray(R.array.states)

        val employeeId = sharedPreferencesManager.employeeId

        licenseViewModel = EmployeeLicenseViewModel(employeeId)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_license, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSpinnerAdapter()
        getAddress(spinner)

        binding.setVariable(BR.license, licenseViewModel)
        activity?.title = "Employee License"

    }

    private fun setSpinnerAdapter() {
        spinner = this.employee_issue_state

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(activity, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.states))
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // Set Adapter to Spinner
        spinner?.adapter = aa

        setSpinner(spinner!!, sharedPreferencesManager.employeeId)

    }

    private fun setSpinner(spinner: Spinner, employeeId: Int) {
        val states = AddressDAO(realm).findById(employeeId)?.state
        spinner.setSelection(licenseViewModel.getIndex(issueState, states))
    }

    private fun getAddress(spinner: Spinner?): String {
        var state = "no state"

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //TODO
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                state = p0?.getItemAtPosition(p2).toString()
                Log.d("Test", state)
            }
        }
        return state
    }

}