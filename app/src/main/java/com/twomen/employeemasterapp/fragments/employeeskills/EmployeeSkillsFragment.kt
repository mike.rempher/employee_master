package com.twomen.employeemasterapp.fragments.employeeskills

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.MainActivity
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.fragments.employeejobinfo.EmployeeJobInfoFragment
import kotlinx.android.synthetic.main.fragment_employee_skills.*

class EmployeeSkillsFragment : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_employee_skills, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        add_skill_fab.setOnClickListener {

            fragmentTransaction(AddEmployeeSkills())
        }
    }
}
