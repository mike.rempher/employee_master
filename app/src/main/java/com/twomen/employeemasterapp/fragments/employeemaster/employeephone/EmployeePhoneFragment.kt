package com.twomen.employeemasterapp.fragments.employeemaster.employeephone

import android.content.Context
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.telephony.PhoneNumberUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.*
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.EMPLOYEE_ID
import com.twomen.employeemasterapp.databinding.FragmentEmployeeNameBinding
import com.twomen.employeemasterapp.databinding.FragmentEmployeePhoneBinding
import com.twomen.employeemasterapp.fragments.employeemaster.employeename.EmployeeNameViewModel
import io.realm.Realm


class EmployeePhoneFragment : BaseFragment() {
    var TAG: String = "EmployeePhoneFragment"

    lateinit var binding: FragmentEmployeePhoneBinding
    private lateinit var phoneViewModel: EmployeePhoneViewModel
    var realm: Realm = Realm.getDefaultInstance()

    private lateinit var employeeTitle: Array<String>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        employeeTitle = resources.getStringArray(R.array.suffix)

        val employeeId = sharedPreferencesManager.employeeId
        phoneViewModel = EmployeePhoneViewModel(employeeId)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_phone, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.setVariable(BR.phone, phoneViewModel)

        activity?.title = "Phone Numbers"
    }
}
