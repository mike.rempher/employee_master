package com.twomen.employeemasterapp.fragments.employeemaster.employeehistory

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryObject
import io.realm.OrderedRealmCollection
import io.realm.Realm
import java.text.SimpleDateFormat
import java.util.*

class EmployeeHistoryAdapter
(private var employeeHistory: OrderedRealmCollection<EmployeeHistoryObject>)
    : RecyclerView.Adapter<EmployeeHistoryAdapter.MyViewHolder>() {
    val TAG = "EmployeeHistoryAdapter"

    private val formatter = SimpleDateFormat("YYYY/MM/DD", Locale.US)
    val realm = Realm.getDefaultInstance()
    var onClickedListener: EmployeeHistoryOnclick? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.employement_history_holder, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int = employeeHistory.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.beginDate.text = formatter.format(employeeHistory[position].beginDate)
        holder.termDate.text = formatter.format(employeeHistory[position].terminationDate)
        holder.termReason.text = employeeHistory[position].terminationReason
        holder.termType.text = employeeHistory[position].isVoluntaryTermination
        val canRehire = employeeHistory[position].canHire
        holder.rehire.isChecked = canRehire

        holder.itemView.callOnClick()
        holder.itemView.setOnClickListener {
            val pos = holder.adapterPosition
            val history = employeeHistory[pos]
            onClickedListener?.onClicked(history)
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var beginDate = itemView.findViewById(R.id.employee_hire_date) as TextView
        var termDate = itemView.findViewById(R.id.employee_term_date) as TextView
        var termReason = itemView.findViewById(R.id.employee_term_reason) as TextView
        var termType = itemView.findViewById(R.id.employee_term_type) as TextView
        var rehire = itemView.findViewById(R.id.rehireCheckBox) as CheckBox
    }
}


