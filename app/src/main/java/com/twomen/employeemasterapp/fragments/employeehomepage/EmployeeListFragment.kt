package com.twomen.employeemasterapp.fragments.employeehomepage

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.EmployeeMaster
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.db.employee.EmployeeObject
import io.realm.Realm
import io.realm.RealmResults

interface EmployeeAdapterOnClick {

    fun onClicked(employee: EmployeeObject)
}

class EmployeeListFragment : BaseFragment(), EmployeeAdapterOnClick {

    lateinit var realm: Realm
    private lateinit var adapter: CustomAdapter
    val layoutManager = LinearLayoutManager(activity)
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        realm = Realm.getDefaultInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_employee_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = activity!!.findViewById(R.id.employee_list)
        recyclerView.layoutManager = layoutManager
        val employeeName: RealmResults<EmployeeObject> = realm.where(EmployeeObject::class.java).findAll()
        adapter = CustomAdapter(employeeName)
        recyclerView.adapter = adapter

        adapter.onClickedListener = this

        activity?.title = "Home BaseData"
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater?) {
        // Inflate the menu items for use in the action bar
        inflater?.inflate(R.menu.employee_search, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onClicked(employee: EmployeeObject) {
        employee.id.let { id ->
            setEmployeeIdPreferences(id)
            fragmentTransaction(EmployeeMaster())
        }
    }
}
