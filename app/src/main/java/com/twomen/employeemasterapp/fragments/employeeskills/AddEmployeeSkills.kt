package com.twomen.employeemasterapp.fragments.employeeskills

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.MainActivity
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.fragments.employeejobinfo.EmployeeJobInfoFragment
import kotlinx.android.synthetic.main.fragment_add_employee_skills.*

class AddEmployeeSkills : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_employee_skills, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        add_new_employee_skill.setOnClickListener {

            fragmentTransaction(EmployeeJobInfoFragment())
        }

        cancel_employee_skill.setOnClickListener {

            fragmentTransaction(EmployeeJobInfoFragment())
        }
    }

}
