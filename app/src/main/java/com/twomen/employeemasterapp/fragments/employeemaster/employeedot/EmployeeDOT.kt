package com.twomen.employeemasterapp.fragments.employeemaster.employeedot

import android.content.Context
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.ISharedPreferencesManager
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.SharedPreferencesManager
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.EMPLOYEE_ID
import com.twomen.employeemasterapp.databinding.FragmentEmployeeDotBinding
import com.twomen.employeemasterapp.databinding.FragmentEmployeeLicenseBinding
import com.twomen.employeemasterapp.fragments.employeemaster.employeelicense.EmployeeLicenseViewModel
import io.realm.Realm

class EmployeeDOT : BaseFragment() {
    var TAG: String = "EmployeeDOT"

    lateinit var binding: FragmentEmployeeDotBinding
    private lateinit var dotViewModel: EmployeeDOTViewModel
    var realm: Realm = Realm.getDefaultInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val employeeId = sharedPreferencesManager.employeeId
        dotViewModel = EmployeeDOTViewModel(employeeId)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_dot, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.setVariable(BR.dot, dotViewModel)
        activity?.title = "Employee DOT"

    }

}
