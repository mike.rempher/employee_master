package com.twomen.employeemasterapp.fragments.employeemaster.employeeaddress

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.ObservableField
import com.twomen.employeemasterapp.BR
import com.twomen.employeemasterapp.db.address.AddressDAO
import io.realm.Realm

class EmployeeAddressViewModel(employeeId: Int) : BaseObservable() {

    val realm: Realm = Realm.getDefaultInstance()
    private val addressDAO: AddressDAO = AddressDAO(realm)

    var addressLine1 = ObservableField<String>()
    var addressLine2 = ObservableField<String>()
    var primaryCity = ObservableField<String>()
    var primaryState = ObservableField<String>()

    init {
        addressLine1.set(addressDAO.findById(employeeId)?.addressLine1 ?: "")
        addressLine2.set(addressDAO.findById(employeeId)?.addressLine2 ?: "")
        primaryCity.set(addressDAO.findById(employeeId)?.city ?: "")
        primaryState.set(addressDAO.findById(employeeId)?.state ?: "")
    }

    //NOTE: ui is using ageString !
    var primaryZipCode: Long = (addressDAO.findById(employeeId)?.zipCode ?: 0)

        set(zip) {
            if (this.primaryZipCode != this.primaryZipCode)
            {
                field = this.primaryZipCode
                notifyPropertyChanged(BR.address)
            }
        }
    //default value
    var zipCodeString: String

        @Bindable
        get() {
            return this.primaryZipCode.toString()
        }
        set(zipCodeString) {
            try
            {
                val `val` = (zipCodeString).toLong()
                this.primaryZipCode = `val`
            }
            catch (ex:NumberFormatException) {
                this.primaryZipCode = 0
            }
        }

    fun getIndex(addressState: Array<String>, name: String?): Int {
        for (i in addressState.indices) {
            if (name == addressState[i]) return i
        }
        return -1
    }



}
