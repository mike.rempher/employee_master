package com.twomen.employeemasterapp.fragments.employeemaster.employeecontactinfo

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.Observable
import android.databinding.ObservableField
import android.telephony.PhoneNumberUtils
import com.twomen.employeemasterapp.BR
import com.twomen.employeemasterapp.db.employee.EmployeeDAO
import io.realm.Realm
import java.util.*

class EmployeeContactViewModel(employeeId: Int): BaseObservable() {

    val realm: Realm = Realm.getDefaultInstance()
    private val employeeDAO: EmployeeDAO = EmployeeDAO(realm)

    var emergancyContactName = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    init{
        emergancyContactName.set(employeeDAO.findById(employeeId)?.emergancyContact ?: "")

    }

    var emergancyContactNumber = (employeeDAO.findById(employeeId)?.emergancyPhone ?: 0)
        set(emergancyContactNumber) {
            if (this.emergancyContactNumber != this.emergancyContactNumber)
            {
                field = this.emergancyContactNumber
                notifyPropertyChanged(BR.emergancy)
            }
        }
    //default value
    var emergancyContactNumberString: String

        @Bindable
        get() {
            return PhoneNumberUtils.formatNumber(emergancyContactNumber.toString(), Locale.getDefault().country)
        }
        set(primaryPhoneString) {
            try
            {
                val `val` = (emergancyContactNumberString).toLong()
                this.emergancyContactNumber = `val`

            }
            catch (ex:NumberFormatException) {
                this.emergancyContactNumber = 0
            }
        }
}