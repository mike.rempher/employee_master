package com.twomen.employeemasterapp.fragments.employeemaster.employeeimportantdates


import android.content.Context
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.*
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.EMPLOYEE_ID
import com.twomen.employeemasterapp.databinding.FragmentEmployeeDatesBinding
import com.twomen.employeemasterapp.databinding.FragmentEmployeeDotBinding
import com.twomen.employeemasterapp.fragments.employeemaster.employeedot.EmployeeDOTViewModel
import io.realm.Realm

class EmployeeImportantDates : BaseFragment() {
    var TAG: String = "EmployeeImportantDates"

    lateinit var binding: FragmentEmployeeDatesBinding
    private lateinit var datesViewModel: EmployeeDatesViewModel
    var realm: Realm = Realm.getDefaultInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val employeeId = sharedPreferencesManager.employeeId
        datesViewModel = EmployeeDatesViewModel(employeeId)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_dates, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.setVariable(BR.dates, datesViewModel)

        activity?.title = "Employee Important Dates"
    }
}
