package com.twomen.employeemasterapp.fragments.employeejobinfo

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.MainActivity
import com.twomen.employeemasterapp.R
import kotlinx.android.synthetic.main.fragment_employee_add_job.*

/**
 * Created by miker on 3/9/18.
 */
class EmployeeAddNewJob: BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_employee_add_job, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.title = "EmployeeObject Job Info"

        add_new_employee_job.setOnClickListener {
            fragmentTransaction(EmployeeJobInfoFragment())
        }

        cancel_employee_job.setOnClickListener {
           fragmentTransaction(EmployeeJobInfoFragment())
        }
    }

}