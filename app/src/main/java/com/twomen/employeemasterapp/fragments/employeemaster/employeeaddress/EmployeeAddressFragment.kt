package com.twomen.employeemasterapp.fragments.employeemaster.employeeaddress

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.twomen.employeemasterapp.BR
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.SharedPreferencesManager
import com.twomen.employeemasterapp.databinding.FragmentEmployeeAddressBinding
import com.twomen.employeemasterapp.db.address.AddressDAO
import com.twomen.employeemasterapp.fragments.employeemaster.employeename.EmployeeNameViewModel
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_employee_address.*

/**
 * Created by miker on 3/5/2018.
 */
class EmployeeAddressFragment: BaseFragment() {
    var TAG: String = "EmployeeAddressFragment"

    lateinit var binding: FragmentEmployeeAddressBinding
    private lateinit var addressViewModel: EmployeeAddressViewModel
    var realm: Realm = Realm.getDefaultInstance()
    private lateinit var employeeState: Array<String>
    private var spinner: Spinner? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true)
        employeeState = resources.getStringArray(R.array.states)

        val employeeId = sharedPreferencesManager.employeeId
        addressViewModel = EmployeeAddressViewModel(employeeId)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_address, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSpinnerAdapter()
        getAddress(spinner)

        binding.setVariable(BR.address, addressViewModel)

        activity?.title = "Address"

    }

    private fun setSpinnerAdapter() {
        spinner = this.employee_state

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(activity, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.states))
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // Set Adapter to Spinner
        spinner?.adapter = aa

        setSpinner(spinner!!, sharedPreferencesManager.employeeId)
    }

    private fun setSpinner(spinner: Spinner, employeeId: Int) {
        val states = AddressDAO(realm).findById(employeeId)?.state
        spinner.setSelection(addressViewModel.getIndex(employeeState, states))
    }

    private fun getAddress(spinner: Spinner?): String {
        var state = "no state"

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //TODO
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                state = p0?.getItemAtPosition(p2).toString()
                Log.d("Test", state)

            }
        }
        return state
    }
}