package com.twomen.employeemasterapp.fragments.employeeactivity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.twomen.employeemasterapp.R

class ActivityToolbar:AppCompatActivity() {

    private var mTopToolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        mTopToolbar = this.findViewById(R.id.employee_activity_toolbar)
        setSupportActionBar(mTopToolbar)

        super.onCreate(savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.back_btn) {

            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
