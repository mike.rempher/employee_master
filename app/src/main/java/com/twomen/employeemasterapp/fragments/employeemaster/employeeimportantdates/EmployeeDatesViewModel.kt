package com.twomen.employeemasterapp.fragments.employeemaster.employeeimportantdates

import android.databinding.BaseObservable
import android.databinding.Observable
import android.databinding.ObservableField
import com.twomen.employeemasterapp.db.employee.EmployeeDAO
import io.realm.Realm
import java.text.SimpleDateFormat
import java.util.*

class EmployeeDatesViewModel(employeeId: Int) : BaseObservable() {
    val realm: Realm = Realm.getDefaultInstance()
    private val employeeDAO: EmployeeDAO = EmployeeDAO(realm)
    private val formatter = SimpleDateFormat("yyyy/MM/dd", Locale.US)

    var birthDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var backgroundCheckDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var drugScreeningDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var dmvReportDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    init {
        birthDate.set(formatter.format(employeeDAO.findById(employeeId)?.birthDate
                ?: ""))
        backgroundCheckDate.set(formatter.format(employeeDAO.findById(employeeId)?.
                criminalHistoryReport ?: ""))
        drugScreeningDate.set(formatter.format(employeeDAO.findById(employeeId)?.dotDrugTestDate
                ?: ""))
        dmvReportDate.set(formatter.format(employeeDAO.findById(employeeId)?.dmvReport
                ?: ""))
    }
}