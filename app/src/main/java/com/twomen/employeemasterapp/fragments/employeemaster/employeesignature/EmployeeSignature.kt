package com.twomen.employeemasterapp.fragments.employeemaster.employeesignature

import android.app.FragmentTransaction
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.databinding.EmployeeSignatureBinding
import kotlinx.android.synthetic.main.employee_signature.*

class EmployeeSignature : BaseFragment() {
    var TAG: String = "EmployeeSignature"
    lateinit var binding: EmployeeSignatureBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.employee_signature, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        this.add_employee_signature.setOnClickListener {
            fragmentTransaction(EmployeeSignatureEntry())
        }
    }
}
