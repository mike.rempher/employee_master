package com.twomen.employeemasterapp.fragments.employeemaster.employeehistory

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.ISharedPreferencesManager
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.SharedPreferencesManager
import com.twomen.employeemasterapp.databinding.FragmentEmploymentHistoryBinding
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryObject
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_employment_history.*


interface EmployeeHistoryOnclick {

    fun onClicked(history: EmployeeHistoryObject)
}

class EmployeeHistory : BaseFragment(), EmployeeHistoryOnclick {

    val TAG: String = "EmployeeHistory"

    lateinit var binding: FragmentEmploymentHistoryBinding
    private lateinit var employeeHistoryViewModel: EmployeeHistoryViewModel
    var employeeId:Int = 0
    var realm: Realm = Realm.getDefaultInstance()
    lateinit var recyclerView: RecyclerView
    private lateinit var adapter: EmployeeHistoryAdapter
    val layoutManager = LinearLayoutManager(activity)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)

        employeeId = sharedPreferencesManager.employeeId
        employeeHistoryViewModel = EmployeeHistoryViewModel(employeeId)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employment_history, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView = activity!!.findViewById(R.id.employee_history_list)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        val history = realm.where(EmployeeHistoryObject::class.java).equalTo("employeeId", employeeId).findAll()

        adapter = EmployeeHistoryAdapter(history)

        recyclerView.adapter = adapter

        adapter.onClickedListener = this

        activity?.title = "Home BaseData"

        add_employee_history.setOnClickListener {
            fragmentTransaction(EmployeeAddHistory())
        }
    }

    override fun onClicked(history: EmployeeHistoryObject) {
        history.id.let { id ->
            setHistoryIdPreferences(id)
            fragmentTransaction(EmployeeAddHistory())
        }
    }
}
