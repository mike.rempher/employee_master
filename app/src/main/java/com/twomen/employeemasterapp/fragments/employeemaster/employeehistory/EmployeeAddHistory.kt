package com.twomen.employeemasterapp.fragments.employeemaster.employeehistory

import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.ISharedPreferencesManager
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.R.id.CancelEmployeeHistory
import com.twomen.employeemasterapp.R.id.term_type_voluntary
import com.twomen.employeemasterapp.SharedPreferencesManager
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.EMPLOYEE_ID
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.HISTORY_ID
import com.twomen.employeemasterapp.databinding.FragmentAddEmployeeHistoryBinding
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryDAO
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryObject
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_add_employee_history.*

class EmployeeAddHistory : BaseFragment() {
    val TAG = "EmployeeAddHistory"

    lateinit var binding: FragmentAddEmployeeHistoryBinding
    lateinit var employeeHistoryViewModel: EmployeeHistoryViewModel
    lateinit var bundle: Bundle
    var employeeId: Int = 0
    var historyId: Int = 0

    var realm: Realm = Realm.getDefaultInstance()
    private val historyDAO: EmployeeHistoryDAO = EmployeeHistoryDAO(realm)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)

        employeeId = sharedPreferencesManager.employeeId
        historyId = sharedPreferencesManager.historyId
        employeeHistoryViewModel = EmployeeHistoryViewModel(historyId)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_employee_history, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        historyId = sharedPreferencesManager.historyId
        setTermType(historyId)

        binding.setVariable(BR.addHistory, employeeHistoryViewModel)


        saveEmployeeHistory.setOnClickListener {
            fragmentTransaction(EmployeeHistory())
        }

        CancelEmployeeHistory.setOnClickListener {
            fragmentTransaction(EmployeeHistory())
        }
    }

    private fun setTermType(historyId: Int) {
        Log.d("TEST", "$historyId")
        val voluntary = historyDAO.findById(historyId)?.isVoluntaryTermination
        if (voluntary == voluntary) {
            term_type_voluntary.isChecked = true
        } else {
            term_type_involuntary.isChecked = true
        }
    }

    fun setRehire(historyId: Int) {

    }

}