package com.twomen.employeemasterapp.fragments.employeemaster.employeehistory

import android.databinding.BaseObservable
import android.databinding.Observable
import android.databinding.ObservableField
import com.twomen.employeemasterapp.db.employeehistory.EmployeeHistoryDAO
import io.realm.Realm
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class EmployeeAddHistoryViewModel(historyId: Int): BaseObservable() {
    val realm: Realm = Realm.getDefaultInstance()
    private val historyDAO: EmployeeHistoryDAO = EmployeeHistoryDAO(realm)
    private val formatter = SimpleDateFormat("yyyy/MM/dd", Locale.US)

    var beginDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var termDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var termReason = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var termType = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var rehire = ObservableField<Boolean>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    init{
        beginDate.set(formatter.format(historyDAO.findById(historyId)?.beginDate ?: ""))
        termDate.set(formatter.format(historyDAO.findById(historyId)?.terminationDate ?: ""))
        termReason.set(historyDAO.findById(historyId)?.terminationReason ?: "")
        termType.set(historyDAO.findById(historyId)?.isVoluntaryTermination ?: "")
        setRehire(historyId)

    }

    fun setRehire(historyId: Int) {
        val canRehire = historyDAO.findById(historyId)?.canHire ?: false
        rehire.set(canRehire)
    }
}