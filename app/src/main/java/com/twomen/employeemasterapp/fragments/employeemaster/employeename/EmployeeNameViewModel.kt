package com.twomen.employeemasterapp.fragments.employeemaster.employeename

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.ObservableField
import com.twomen.employeemasterapp.db.employee.EmployeeDAO
import io.realm.Realm
import com.twomen.employeemasterapp.BR



class EmployeeNameViewModel(employeeId: Int) : BaseObservable() {

    val realm: Realm = Realm.getDefaultInstance()
    private val employeeDAO: EmployeeDAO = EmployeeDAO(realm)

    var firstName = ObservableField<String>()
    var middleName = ObservableField<String>()
    var lastName = ObservableField<String>()
    var initials = ObservableField<String>()
    var title = ObservableField<String>()


    init {
        title.set(employeeDAO.findById(employeeId)?.title ?: "")
        firstName.set(employeeDAO.findById(employeeId)?.firstname ?: "")
        middleName.set(employeeDAO.findById(employeeId)?.middleName ?: "")
        lastName.set(employeeDAO.findById(employeeId)?.lastname ?: "")
        initials.set(employeeDAO.findById(employeeId)?.quickBooksInitials ?: "")
    }

    //NOTE: ui is using ageString !
    var payId:Int = (employeeDAO.findById(employeeId)?.payId ?: 0)

        set(age) {
            if (this.payId != payId)
            {
                field = payId
                notifyPropertyChanged(BR.employee)
            }
        }
    //default value
    var payIdString:String

        @Bindable
        get() {
            return Integer.toString(this.payId)
        }
        set(payIdString) {
            try
            {
                val `val` = Integer.parseInt(payIdString)
                this.payId = `val`
            }
            catch (ex:NumberFormatException) {
                this.payId = 0
            }
        }


    fun getIndex(employeeTitle: Array<String>, name: String?): Int {
        for (i in employeeTitle.indices) {
            if (name == employeeTitle[i]) return i
        }
        return -1
    }

}



