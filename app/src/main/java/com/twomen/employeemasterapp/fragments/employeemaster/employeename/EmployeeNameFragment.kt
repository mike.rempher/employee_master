package com.twomen.employeemasterapp.fragments.employeemaster.employeename

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.twomen.employeemasterapp.*
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.EMPLOYEE_ID
import com.twomen.employeemasterapp.SharedPreferencesManager.Companion.PREFS_FILENAME
import com.twomen.employeemasterapp.databinding.FragmentEmployeeNameBinding
import com.twomen.employeemasterapp.db.employee.EmployeeDAO
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_employee_name.*
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.R.id.tabs
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import kotlinx.android.synthetic.main.employee_master.*


class EmployeeNameFragment : BaseFragment() {
    var TAG: String = "EmployeeNameFragment"

    lateinit var binding: FragmentEmployeeNameBinding
    private lateinit var employeeNameViewModel: EmployeeNameViewModel
    var realm: Realm = Realm.getDefaultInstance()
    private lateinit var employeeTitle: Array<String>
    private var spinner: Spinner? = null    //This is our viewPager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        employeeTitle = resources.getStringArray(R.array.suffix)

            val employeeId = sharedPreferencesManager.employeeId
            employeeNameViewModel = EmployeeNameViewModel(employeeId)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_name, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getTitle(spinner)
        setSpinnerAdapter()

        binding.setVariable(BR.employee, employeeNameViewModel)

        activity?.title = "Employee Name"
    }

    private fun setSpinnerAdapter() {
        spinner = this.employee_title

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(activity, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.suffix))

        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // Set Adapter to Spinner
        spinner?.adapter = aa

        setSpinner(spinner!!, sharedPreferencesManager.employeeId)

    }

    private fun setSpinner(spinner: Spinner, employeeId: Int) {
        val title = EmployeeDAO(realm).findById(employeeId)?.title
        spinner.setSelection(employeeNameViewModel.getIndex(employeeTitle, title))
    }

    private fun getTitle(spinner: Spinner?): String {
        var title = "no title"

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //TODO
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                title = p0?.getItemAtPosition(p2).toString()
            }
        }
        return title
    }
}

