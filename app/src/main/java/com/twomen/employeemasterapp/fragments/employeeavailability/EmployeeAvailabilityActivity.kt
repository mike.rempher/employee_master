package com.twomen.employeemasterapp.fragments.employeeavailability

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.twomen.employeemasterapp.ExpandableListAdapter
import com.twomen.employeemasterapp.ISharedPreferencesManager
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.SharedPreferencesManager
import com.twomen.employeemasterapp.databinding.FragmentEmployeeAddressBinding
import com.twomen.employeemasterapp.db.address.AddressDAO
import com.twomen.employeemasterapp.fragments.employeemaster.employeeaddress.EmployeeAddressViewModel
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_employee_address.*
import kotlinx.android.synthetic.main.fragment_employee_time_off_request.*

class EmployeeAvailabilityActivity: AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var TAG: String = "EmployeeAvailabilityActivity"

    private var listOfItems = arrayOf(
            "Vacation",
            "Suspension",
            "Sick Leave",
            "Sabbatical",
            "FMLA - Paternity",
            "FMLA - Maternity",
            "FMLA - Health",
            "FMLA - Grieving",
            "Alcohol/Drug Treatment",
            "Military Leave/Drill",
            "Jury Duty")

    lateinit var spinner: Spinner


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        request_reason.onItemSelectedListener = this

        spinner = this.request_reason
        spinner.onItemSelectedListener = this

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, listOfItems)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner.adapter = aa
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {
    }
}