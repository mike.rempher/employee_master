package com.twomen.employeemasterapp.fragments.employeemaster.employeephone

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.telephony.PhoneNumberUtils
import com.twomen.employeemasterapp.BR
import com.twomen.employeemasterapp.db.employee.EmployeeDAO
import io.realm.Realm
import java.util.*

class EmployeePhoneViewModel(employeeId: Int) : BaseObservable() {

    val realm: Realm = Realm.getDefaultInstance()
    private val employeeDAO: EmployeeDAO = EmployeeDAO(realm)

//    var primaryPhone = ObservableField<Long>()
//    var primaryExt = ObservableField<Int>()
//    var secondaryPhone = ObservableField<Long>()
//    var secondaryExt = ObservableField<Int>()

    //NOTE: ui is using primaryExtString !
    var primaryExt: Int = (employeeDAO.findById(employeeId)?.primaryPhoneExt ?: 0)
        set(primaryExt) {
            if (this.primaryExt != primaryExt) {
                field = this.primaryExt
                notifyPropertyChanged(BR.phone)
            }
        }

    //default value
    var primaryExtString: String
        @Bindable
        get() {
            return Integer.toString(this.primaryExt)
        }
        set(primaryExtString) {
            try {
                val `val` = Integer.parseInt(primaryExtString)
                this.primaryExt = `val`
                PhoneNumberUtils.formatNumber(primaryExt.toString(), "United States")
            } catch (ex: NumberFormatException) {
                this.primaryExt = 0
            }
        }


    var secondaryExt: Int = (employeeDAO.findById(employeeId)?.secondaryPhoneExt ?: 0)
        set(secondaryExt) {
            if (this.secondaryExt != secondaryExt) {
                field = this.secondaryExt
                notifyPropertyChanged(BR.phone)
            }
        }

    var secondaryExtString: String
        @Bindable
        get() {
            return Integer.toString(this.secondaryExt)
        }
        set(secondaryExtString) {
            try {
                val `val` = Integer.parseInt(secondaryExtString)
                this.secondaryExt = `val`
            } catch (ex: NumberFormatException) {
                this.secondaryExt = 0
            }
        }


    var primaryPhone: Long = (employeeDAO.findById(employeeId)?.primaryPhone ?: 0)
        set(primaryPhone) {
            if (this.primaryPhone != primaryPhone) {
                field = this.primaryPhone
                notifyPropertyChanged(BR.address)
            }
        }
    //default value
    var primaryPhoneString: String
        @Bindable
        get() {
            return PhoneNumberUtils.formatNumber(primaryPhone.toString(), Locale.getDefault().country)
        }
        set(primaryPhoneString) {
            try {
                val `val` = (primaryPhoneString).toLong()
                this.primaryPhone = `val`

            } catch (ex: NumberFormatException) {
                this.primaryPhone = 0
            }
        }


    var secondaryPhone: Long = (employeeDAO.findById(employeeId)?.secondaryPhone ?: 0)
        set(secondaryPhone) {
            if (this.secondaryPhone != secondaryPhone) {
                field = this.secondaryPhone
                notifyPropertyChanged(BR.address)
            }
        }
    //default value

    var secondaryPhoneString: String
        @Bindable
        get() {
            return PhoneNumberUtils.formatNumber(secondaryPhone.toString(), Locale.getDefault().country)
        }
        set(secondaryPhoneString) {
            try {
                val `val` = (secondaryPhoneString).toLong()
                this.secondaryPhone = `val`
            } catch (ex: NumberFormatException) {
                this.secondaryPhone = 0
            }
        }
}