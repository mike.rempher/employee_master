package com.twomen.employeemasterapp.fragments.employeeavailability

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import android.widget.Toast
import com.twomen.employeemasterapp.BaseFragment
import com.twomen.employeemasterapp.ExpandableListAdapter
import kotlinx.android.synthetic.main.fragment_employee_availability.*

class EmployeeAvailabilityFragment : BaseFragment() {

    private lateinit var listAdapter: ExpandableListAdapter

    private val employeeTimeOff = EmployeeTimeOff()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        listAdapter = ExpandableListAdapter(activity!!.applicationContext)

        findFocus()
        return inflater.inflate(com.twomen.employeemasterapp.R.layout.fragment_employee_availability, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // setting list adapter
        expandable_listview.setAdapter(listAdapter)
    }

    private fun findFocus(): View {
        return activity!!.currentFocus
    }
}



