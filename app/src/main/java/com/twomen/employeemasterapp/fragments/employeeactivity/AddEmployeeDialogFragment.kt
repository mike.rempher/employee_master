package com.twomen.employeemasterapp.fragments.employeeactivity

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.DatePickerFragment
import com.twomen.employeemasterapp.MainActivity
import com.twomen.employeemasterapp.R
import com.twomen.employeemasterapp.fragments.employeejobinfo.EmployeeJobInfoFragment
import kotlinx.android.synthetic.main.add_empoloyee_activity.*
import kotlinx.android.synthetic.main.employee_activity_toolbar.*


open class AddEmployeeDialogFragment : DialogFragment() {

    private val DATE_DIALOG_ID = 0
    private var mYear: Int = 0
    private var mMonth: Int = 0
    private var mDay: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.add_empoloyee_activity, container, false)
        setHasOptionsMenu(false)
        return v!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        back_btn.setOnClickListener {
            (activity as MainActivity).fragmentTransaction(EmployeeActivityFragment())
        }

        begin_activity_date.setOnClickListener {
            (activity as MainActivity).fragmentTransaction(DatePickerFragment())
        }


        super.onViewCreated(view, savedInstanceState)
    }


    companion object {
        const val TAG = "AddEmployeeDialogFragment"
    }

    override fun getTheme(): Int {
        return R.style.AppTheme
    }

    private fun onCreateDialog(id: Int): Dialog? {
        when (id) {
            DATE_DIALOG_ID -> return DatePickerDialog(activity,
                    mDateSetListener,
                    mYear, mMonth, mDay)
        }
        return null
    }

    private val mDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        mYear = year
        mMonth = monthOfYear
        mDay = dayOfMonth
        begin_activity_date.setText(StringBuilder().append(mDay).append("/").append(mMonth + 1).append("/").append(mYear))
    }

}
