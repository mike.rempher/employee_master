package com.twomen.employeemasterapp.fragments.employeemaster.employeedot

import android.databinding.BaseObservable
import android.databinding.Observable
import android.databinding.ObservableField
import com.twomen.employeemasterapp.db.address.AddressDAO
import com.twomen.employeemasterapp.db.employee.EmployeeDAO
import io.realm.Realm
import java.text.SimpleDateFormat
import java.util.*

class EmployeeDOTViewModel(employeeId: Int) : BaseObservable() {

    val realm: Realm = Realm.getDefaultInstance()
    private val employeeDAO: EmployeeDAO = EmployeeDAO(realm)
    private val formatter = SimpleDateFormat("yyyy/MM/dd", Locale.US)

    var dotCheckBox = ObservableField<Boolean>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var dotExaminationDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }

    var dotExpirationDate = ObservableField<String>().apply {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })
    }


    init {
        dotExaminationDate.set(formatter.format(employeeDAO.findById(employeeId)?.dotExaminationDate
                ?: ""))
        dotExpirationDate.set(formatter.format(employeeDAO.findById(employeeId)?.dotExpirationDate
                ?: ""))
        setDOT(employeeId)
    }

    private fun setDOT(employeeId: Int) {
        val hasDOT = employeeDAO.findById(employeeId)?.hasDOTPhysicalCard ?: false
        dotCheckBox.set(hasDOT)
    }


}