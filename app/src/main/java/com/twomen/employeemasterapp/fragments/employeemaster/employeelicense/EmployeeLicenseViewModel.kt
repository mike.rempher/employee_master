package com.twomen.employeemasterapp.fragments.employeemaster.employeelicense

import android.databinding.BaseObservable
import android.databinding.Observable
import android.databinding.ObservableField
import android.util.Log
import android.widget.CheckBox
import android.widget.Checkable
import com.twomen.employeemasterapp.R.id.employee_chauffeur_chk
import com.twomen.employeemasterapp.db.employee.EmployeeDAO
import io.realm.Realm
import java.text.SimpleDateFormat
import java.util.*

class EmployeeLicenseViewModel(employeeId: Int): BaseObservable() {

    val realm: Realm = Realm.getDefaultInstance()
    private val employeeDAO: EmployeeDAO = EmployeeDAO(realm)
    private val formatter = SimpleDateFormat("yyyy/MM/dd", Locale.US)

    var licenseNumber = ObservableField<String>()
    var issueDate = ObservableField<String>()
    var expireDate = ObservableField<String>()
    var chauffeurCheckBox = ObservableField<Boolean>().apply {
        addOnPropertyChangedCallback(object: Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })

    }
    var cdlCheckBox = ObservableField<Boolean>().apply {
        addOnPropertyChangedCallback(object: Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                //TODO
            }
        })

    }

    init {
        licenseNumber.set(employeeDAO.findById(employeeId)?.driversLicense ?: "")
        issueDate.set(formatter.format(employeeDAO.findById(employeeId)?.driversLicenseIssueDate
                ?: ""))
        expireDate.set(formatter.format(employeeDAO.findById(employeeId)?.driversLicenseExpireDate
                ?: ""))
        setChauffeur(employeeId)
        setCDL(employeeId)
    }

    fun getIndex(employeeTitle: Array<String>, name: String?): Int {
        for (i in employeeTitle.indices) {
            if (name == employeeTitle[i]) return i
        }
        return -1
    }

    private fun setChauffeur(employeeId: Int) {
        val hasChauffeur = employeeDAO.findById(employeeId)?.hasChauffeurEndorsement ?: false
        chauffeurCheckBox.set(hasChauffeur)
    }

    private fun setCDL(employeeId: Int) {
        val hasCDL = employeeDAO.findById(employeeId)?.hasCDL ?: false
        cdlCheckBox.set(hasCDL)
    }
}