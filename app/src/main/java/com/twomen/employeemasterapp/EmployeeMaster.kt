package com.twomen.employeemasterapp

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.databinding.EmployeeMasterBinding
import com.twomen.employeemasterapp.fragments.employeemaster.employeehistory.EmployeeAddHistory
import com.twomen.employeemasterapp.fragments.employeemaster.employeehistory.EmployeeHistory
import io.realm.Realm
import kotlinx.android.synthetic.main.employee_master.*

class EmployeeMaster : BaseFragment() {
    val TAG: String = "EmployeeMaster"

    val realm: Realm = Realm.getDefaultInstance()
    lateinit var binding: EmployeeMasterBinding
    var employeeId: Int = 0
    var fragment: Fragment? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fragment = getTopFragment()
        if (getTopFragment() == EmployeeAddHistory()) {
            fragmentTransaction(EmployeeHistory())
        }

        binding = DataBindingUtil.inflate(inflater, R.layout.employee_master, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        hideItem()
    }

    private fun hideItem() {
        val navigationView = activity?.findViewById(R.id.nav_view) as NavigationView
        val navMenu = navigationView.menu
        val fab = activity?.findViewById(R.id.add_employee_fab) as FloatingActionButton

        fab.visibility = View.GONE
        navMenu.findItem(R.id.nav_home).isVisible = true
        navMenu.findItem(R.id.nav_employee_master).isVisible = false
        navMenu.findItem(R.id.nav_availability).isVisible = true
        navMenu.findItem(R.id.nav_job_info).isVisible = true
        navMenu.findItem(R.id.nav_activity_type).isVisible = true
        navMenu.findItem(R.id.nav_employee_skills).isVisible = true
        navMenu.findItem(R.id.nav_employee_details).isVisible = true
    }

    override fun onStart() {
        super.onStart()
        configureTabLayout()
    }

    fun configureTabLayout() {
        tab_layout.addTab(tab_layout.newTab().setText("Name"))
        tab_layout.addTab(tab_layout.newTab().setText("Address"))
        tab_layout.addTab(tab_layout.newTab().setText("Phone"))
        tab_layout.addTab(tab_layout.newTab().setText("License"))
        tab_layout.addTab(tab_layout.newTab().setText("DOT"))
        tab_layout.addTab(tab_layout.newTab().setText("Dates"))
        tab_layout.addTab(tab_layout.newTab().setText("Contact"))
        tab_layout.addTab(tab_layout.newTab().setText("Hist."))
        tab_layout.addTab(tab_layout.newTab().setText("Sig."))

        val adapter = TabPagerAdapter(activity!!.supportFragmentManager, tab_layout.tabCount)
        pager.adapter = adapter

        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tab_layout))
        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun getTopFragment(): Fragment? {
        if (activity?.supportFragmentManager?.backStackEntryCount == 0) {
            return null
        }
        val fragmentTag = activity?.supportFragmentManager?.getBackStackEntryAt(activity!!.supportFragmentManager.backStackEntryCount - 1)?.name
        return activity?.supportFragmentManager?.findFragmentByTag(fragmentTag)
    }
}