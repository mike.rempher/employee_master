package com.twomen.employeemasterapp

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twomen.employeemasterapp.fragments.employeeactivity.AddEmployeeDialogFragment
import com.twomen.employeemasterapp.fragments.employeejobinfo.EmployeeJobInfoFragment
import kotlinx.android.synthetic.main.employee_activity_toolbar.*

class DatePickerFragment : DialogFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_date_picker, container, false)
        setHasOptionsMenu(false)
        return v!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        back_btn.setOnClickListener {

            (activity as MainActivity).fragmentTransaction(AddEmployeeDialogFragment())
        }
    }
}