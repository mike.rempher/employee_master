package com.twomen.employeemasterapp

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.twomen.employeemasterapp.fragments.employeemaster.employeeaddress.EmployeeAddressFragment
import com.twomen.employeemasterapp.fragments.employeemaster.employeecontactinfo.EmployeeContactInfo
import com.twomen.employeemasterapp.fragments.employeemaster.employeedot.EmployeeDOT
import com.twomen.employeemasterapp.fragments.employeemaster.employeehistory.EmployeeHistory
import com.twomen.employeemasterapp.fragments.employeemaster.employeeimportantdates.EmployeeImportantDates
import com.twomen.employeemasterapp.fragments.employeemaster.employeelicense.EmployeeLicenseFragment
import com.twomen.employeemasterapp.fragments.employeemaster.employeename.EmployeeNameFragment
import com.twomen.employeemasterapp.fragments.employeemaster.employeephone.EmployeePhoneFragment
import com.twomen.employeemasterapp.fragments.employeemaster.employeesignature.EmployeeSignature

class TabPagerAdapter(
        fm: FragmentManager,
        private var tabCount: Int) :
        FragmentPagerAdapter(fm) {

    var fragments = ArrayList<Fragment>()

    override fun getItem(position: Int): Fragment? =
        when (position) {
            0 -> EmployeeNameFragment()
            1 -> EmployeeAddressFragment()
            2 -> EmployeePhoneFragment()
            3 -> EmployeeLicenseFragment()
            4 -> EmployeeDOT()
            5 -> EmployeeImportantDates()
            6 -> EmployeeContactInfo()
            7 -> EmployeeHistory()
            8 -> EmployeeSignature()
            else -> null
        }

    override fun getCount(): Int {
        return tabCount
    }
}

