package com.twomen.employeemasterapp

import android.app.Application
import io.realm.Realm

class EmployeeMasterApp: Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
}