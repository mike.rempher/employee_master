package com.twomen.employeemasterapp

interface ISharedPreferencesManager {
    var employeeId: Int
    var historyId: Int
}