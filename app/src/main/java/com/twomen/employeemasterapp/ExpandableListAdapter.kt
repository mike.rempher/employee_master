package com.twomen.employeemasterapp

import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.twomen.employeemasterapp.databinding.FragmentEmployeeCurrentAvailabilityBinding
import com.twomen.employeemasterapp.databinding.FragmentEmployeeTimeOffRequestBinding

class ExpandableListAdapter(private val _context: Context
) : BaseExpandableListAdapter() {

    override fun getChild(groupPosition: Int, childPosititon: Int): Any {
        return (groupPosition)
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int,
                              isLastChild: Boolean, view: View?, parent: ViewGroup): View? {
        var convertView = view

        convertView = when (groupPosition) {
            0 -> {
                val binding: FragmentEmployeeCurrentAvailabilityBinding
                // 1st view specifics here
                val inflater: LayoutInflater = LayoutInflater.from(_context)

                binding = DataBindingUtil.inflate(inflater,
                        com.twomen.employeemasterapp.R.layout.fragment_employee_current_availability,
                        parent,
                        false)

                return binding.root
            }
            1 -> {
                val binding: FragmentEmployeeTimeOffRequestBinding
                // 1st view specifics here
                val inflater: LayoutInflater = LayoutInflater.from(_context)

                binding = DataBindingUtil.inflate(inflater,
                        com.twomen.employeemasterapp.R.layout.fragment_employee_time_off_request,
                        parent,
                        false)

                return binding.root
            }
            else -> null
        }
        return convertView
    }


    override fun onGroupExpanded(groupPosition: Int) {
        when (groupPosition) {
            0 -> {
            }
            1 -> {
            }
        }

        super.onGroupExpanded(groupPosition)
    }


    override fun getChildTypeCount(): Int {
        return 2
    }

    override fun getChildType(groupPosition: Int, childPosition: Int): Int {
        return if (childPosition == 0)
            0
        else
            1
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return 1
    }

    override fun getGroup(groupPosition: Int): Any {

        return groupPosition
    }

    override fun getGroupCount(): Int {
        return 2
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                              view: View?, parent: ViewGroup): View {

        var convertView = view

        if (convertView == null) {
            val inflater = this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.list_group, parent, false)
        }

        val lblListHeader = convertView!!
                .findViewById(R.id.lblListHeader) as TextView
        lblListHeader.setTypeface(null, Typeface.BOLD)
        lblListHeader.setPadding(24, 0, 0, 0)
        if (groupPosition == 0) {
            lblListHeader.text = _context.getString(R.string.current_availability)
        } else if (groupPosition == 1) {
            lblListHeader.text = _context.getString(R.string.time_off)
        }
        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun getCombinedChildId(groupId: Long, childId: Long): Long {
        return 0
    }

    override fun getCombinedGroupId(groupId: Long): Long {
        return 0
    }
}